var app = angular.module('myApp', []);

app.run(function ($rootScope) {
    $rootScope.firstName = 'Nga';
});

app.controller('1stController', function($scope) {
    $scope.myEmailModel = 'dongsihoanglan@gmail.com';
    $scope.firstName = "Lan";
    $scope.lastName = "Dong";
    $scope.empSalary = 10000000;
    
    $scope.updateEmailAddress = function() {
        $scope.myEmailModel = 'geraintdong@gmail.com';
    };
    $scope.fullName = function(first, last) {
        return first + " " + last;
    };
});